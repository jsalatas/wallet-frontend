import { combineReducers } from 'redux';
import login from './pages/login/reducers';
import user, { wallet } from './layouts/reducers';
import currencies_value from 'components/widgets/CurrenciesValue/reducers';
import last_transactions from 'components/widgets/LastTransactions/reducers'
import earnings from 'components/widgets/Earnings/reducers'
import month_earnings from 'components/widgets/MonthEarnings/reducers'
import transactions from 'components/widgets/Transactions/reducers'
import btc_in from 'components/widgets/WalletActions/CashIn/methods/BTC/reducers'
import fac_in from 'components/widgets/WalletActions/CashIn/methods/FAC/reducers'

const reducers = {
  currencies_value,
  earnings,
  last_transactions,
  login,
  month_earnings,
  user,
  wallet,
  transactions,
  btc_in,
  fac_in
};


export default combineReducers(reducers);