
export const tokenName = 'auth';

export function getAuthToken() {
  if (!window.localStorage[tokenName]) {
    return undefined;
  }
  return window.localStorage[tokenName];
}

export function logOut() {
  window.localStorage.removeItem(tokenName);
}


export function isLoggedIn() {
  return Boolean(getAuthToken());
}


export function requireAuth(nextState, replace) {
  if (!isLoggedIn()) {
    replace('/');
  }
}