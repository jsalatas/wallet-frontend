import  USER, { WALLET } from './constants';

export default function user(state = { status: 'initial' }, action) {
  switch (action.type) {
    case USER : {
      const { 
        user = state.data,
        error = state.error,
        status = state.status,
       } = action.payload
      return {
        data: user,
        error,
        status,
      };
    }
    default:
      return state;
  }
}

export function wallet(state = { status: 'initial', data: []}, action) {
  switch (action.type) {
    case WALLET : {
      const { 
        wallet = state.data,
        error = state.error,
        status = state.status,
       } = action.payload
      return {
        data: wallet,
        error,
        status,
      };
    }
    default:
      return state;
  }
}