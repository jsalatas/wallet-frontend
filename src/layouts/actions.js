import { createAction } from 'redux-actions';
import { getAuthToken, logOut } from 'utils';
import axios from 'axios';
import USER, { WALLET } from './constants';


const reset = createAction(USER, () => ({
  status: 'initial',
}));

const begin = createAction(USER, () => ({
  status: 'pending',
}));

const fail = createAction(USER, error => ({
  error,
  status: 'error',
}));

const success = createAction(USER, user => ({
  user,
  status: 'success'
}));

const resetWallet = createAction(WALLET, () => ({
  status: 'initial',
}));

const beginWallet = createAction(WALLET, () => ({
  status: 'pending',
}));

const failWallet = createAction(WALLET, error => ({
  error,
  status: 'error',
}));

const successWallet = createAction(WALLET, wallet=> ({
  wallet,
  status: 'success'
}));

const getMe = () => 
  (dispatch) => {
  dispatch(begin())
  dispatch(beginWallet())
  Promise.all(
    [axios({
      method: 'GET',
      headers: {
        Authorization: `Bearer ${getAuthToken()}`
      },
      url: 'https://api.bankofthecommons.coop/user/v1/account'
    }),
    axios({
      method: 'GET',
      headers: {
        Authorization: `Bearer ${getAuthToken()}`
      },
      url: 'https://api.bankofthecommons.coop/user/v1/wallet'
    })]
  ).then(responses => {
      var user = responses[0];
      var wallet = responses[1];
      dispatch(success(user.data.data))
      dispatch(successWallet(wallet.data.data))
  }).catch(error => {
    logOut()
    dispatch(fail(error));
    dispatch(failWallet(error));
  });
 
}

const actions = {
  reset,
  begin,
  success,
  fail,
  resetWallet,
  beginWallet,
  successWallet,
  failWallet,
  getMe
};

export default actions;