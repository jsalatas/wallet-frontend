import React, { Component } from 'react'
import WalletBalance from "components/widgets/WalletBalance";
import Transactions from 'components/widgets/Transactions'
import WalletActions from 'components/widgets/WalletActions'

export default class Wallet extends Component {
  render() {
    return (
      <div>
        <WalletBalance />
        <WalletActions />
        <Transactions />
      </div>
    )
  }
}
