import React from "react";
import PropTypes from "prop-types";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";


import dashboardStyle from "assets/jss/material-dashboard-pro-react/views/dashboardStyle";
import CurrenciesValue from "components/widgets/CurrenciesValue";
import Earnings from "components/widgets/Earnings"
import MonthEarnings from "components/widgets/MonthEarnings"
import LastTransactions from "components/widgets/LastTransactions";
import GridContainer from "components/atoms/Grid/GridContainer.jsx";
import GridItem from "components/atoms/Grid/GridItem.jsx";

class Dashboard extends React.Component {

  render() {
    return (
      <div>
        <CurrenciesValue />
        <MonthEarnings />
        <GridContainer
          direction="row"
          justify="space-between"
          alignItems="flex-start"
          container
        >
          <GridItem xs={7}>
            <LastTransactions />
          </GridItem>
          <GridItem xs={5}>
            <Earnings />
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};


export default withStyles(dashboardStyle)(Dashboard);