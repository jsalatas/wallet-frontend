import { createAction } from 'redux-actions';
import { tokenName, getAuthToken } from '../../utils';
import axios from 'axios';
import LOGIN, { client_id, client_secret } from './constants';


const reset = createAction(LOGIN, () => ({
  status: 'initial',
}));

const begin = createAction(LOGIN, () => ({
  status: 'pending',
}));

const success = createAction(LOGIN, token => {
  window.localStorage[tokenName] = token;
  return {
    token: token,
    status: 'success',
  };
});

const fail = createAction(LOGIN, error => ({
  error,
  status: 'error',
}));

const success_get_me = createAction(LOGIN, user => ({
  user,
  status: 'success_get_me'
}));

const getMe = () => 
  (dispatch) => {
  axios({
    method: 'GET',
    headers: {
      Authorization: `Bearer ${getAuthToken()}`
    },
    url: 'https://api.bankofthecommons.coop/user/v1/account'
  }).then(response => {
      if (response.status >= 400) {
        const error = new Error(response.statusText);
        error.response = response;
        throw error;
      }
      let user = response.data
      dispatch(success_get_me(user))
  }).catch(error => {
      dispatch(error(error))
  })
}


const login = (username, password, code) =>
  (dispatch) => {
    dispatch(begin());
    let body = {
        client_id,
        client_secret,
        pin: code,
        grant_type: "password",
        scope: "panel",
        username,
        password
     }
    console.log(`[debug] Fetch login: ${JSON.stringify(body)}`)
    axios({
      url: 'https://api.bankofthecommons.coop/oauth/v3/token',
      method: 'POST',
      data: body
    }).then(response => {
      let user = response.data
      dispatch(success(user.access_token))
    }).catch(error => {
      dispatch(fail(error.response.data));
    });
  };


const actions = {
  reset,
  begin,
  success,
  fail,
  login,
  getMe
};

export default actions;