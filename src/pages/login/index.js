import React from "react";
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import actions from './actions';
import { isLoggedIn } from '../../utils';


// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";

// @material-ui/icons
import Email from "@material-ui/icons/Email";
// import LockOutline from "@material-ui/icons/LockOutline";

// core components
import GridContainer from "components/atoms/Grid/GridContainer.jsx";
import GridItem from "components/atoms/Grid/GridItem.jsx";
import CustomInput from "components/atoms/CustomInput/CustomInput.jsx";
import Button from "components/atoms/CustomButtons/Button.jsx";
import Card from "components/atoms/Card/Card.jsx";
import CardBody from "components/atoms/Card/CardBody.jsx";
import CardHeader from "components/atoms/Card/CardHeader.jsx";
import CardFooter from "components/atoms/Card/CardFooter.jsx";
import CircularProgress from '@material-ui/core/CircularProgress';

import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import loginPageStyle from "assets/jss/material-dashboard-pro-react/views/loginPageStyle.jsx";

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      cardAnimaton: "cardHidden",
      code: '',
      loading: false,
      password: '',
      username: '',
      snackbar: false
    };
    this.handleUsernameChange = (event) => {
      this.setState({username: event.target.value})
    }
    this.handlePasswordChange = (event) => {
      this.setState({password: event.target.value})
    }
    this.handleCodeChange = (event) => {
      this.setState({code: event.target.value})
    }
    this.handleSubmit = (event) => {
      event.preventDefault();
      this.setState({loading: true})
      let { username, password, code} = this.state;
      this.props.login(username, password, code);
    };
    this.handleCloseSnack = (event) => {
      this.setState({snackbar: false})
    }
  }

  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    this.timeOutFunction = setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }
  componentWillMount() {
    if (isLoggedIn()) {
      this.props.history.push('/dashboard')
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.status === 'error') {
      this.setState({ loading: false, status: 'error', message: nextProps.error.error_description, snackbar: true });
    } else if (nextProps.status === 'success') {
      this.props.history.push('/dashboard')
    }
  }
  shouldComponentUpdate() {
    if (isLoggedIn()) {
      this.props.history.push('/dashboard')
      return false;
    }
    return true;
  }

  componentWillUnmount() {
    clearTimeout(this.timeOutFunction);
    this.timeOutFunction = null;
  }
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.container}>
        <GridContainer justify="center">
          <GridItem xs={12} sm={6} md={4}>
            <form onSubmit={this.handleSubmit}>
              <Card login className={classes[this.state.cardAnimaton]}>
                <CardHeader
                  className={`${classes.cardHeader} ${classes.textCenter}`}
                  color="primary"
                >
                  <h4 className={classes.cardTitle}>Log in</h4>
                </CardHeader>
                <CardBody>
                  <CustomInput
                    labelText="Username"
                    id="username"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      required: true,
                      onChange: this.handleUsernameChange,
                      endAdornment: (
                        <InputAdornment position="end">
                          <Email className={classes.inputAdornmentIcon} />
                        </InputAdornment>
                      )
                    }}
                  />
                  <CustomInput
                    labelText="Password"
                    id="password"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      required: true,
                      onChange: this.handlePasswordChange,
                      type: 'password',
                      endAdornment: (
                        <InputAdornment position="end">
                          <Icon className={classes.inputAdornmentIcon}>
                            lock_outline
                          </Icon>
                        </InputAdornment>
                      )
                    }}
                  />
                  <CustomInput
                    labelText="Code"
                    id="code"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      onChange: this.handleCodeChange,
                      type: 'code',
                      endAdornment: (
                        <InputAdornment position="end">
                          <Icon className={classes.inputAdornmentIcon}>
                            vpn_key_outline
                          </Icon>
                        </InputAdornment>
                      )
                    }}
                  />

                </CardBody>
                <CardFooter className={classes.justifyContentCenter}>
                {
                  this.state.loading ? 
                  <CircularProgress />
                  :
                  <Button type="submit" color="primary" simple size="lg" block onSubmit={this.handleSubmit}>
                    Log In
                  </Button>
                }
                </CardFooter>
              </Card>
            </form>
          </GridItem>
        </GridContainer>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          open={this.state.snackbar}
          autoHideDuration={6000}
          onClose={this.handleCloseSnack}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{this.state.message}</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleCloseSnack}
            >
              <CloseIcon />
            </IconButton>,
          ]}
        />
      </div>
    );
  }
}

LoginPage.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
  token: state.login.token,
  status: state.login.status,
  error: state.login.error,
});


export default connect(mapStateToProps, actions)(withStyles(loginPageStyle)(LoginPage));