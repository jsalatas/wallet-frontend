import  LOGIN from './constants';

export default function login(state = { status: 'initial' }, action) {
  switch (action.type) {
    case LOGIN : {
      const { 
        token = state.token,
        error = state.error,
        status = state.status,
       } = action.payload
      return {
        token, 
        error,
        status,
      };
    }
    default:
      return state;
  }
}