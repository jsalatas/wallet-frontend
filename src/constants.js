export const postLogoutURL = '/';

export const errors = {
  login: {
    UNAUTHORIZED: 'Unable to log you in. Wrong email or password',
    ERROR: 'Something went wrong'
  },
};

export const apiUrl = 'http://localhost:8000/api/v1';