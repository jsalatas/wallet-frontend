import React from "react";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import Login from '../pages/login';
import Dashboard from '../layouts/Dashboard';


const hist = createBrowserHistory();

const routes = [
    { path: '/login', name: 'Login', component: Login },
    { path: "/", name: "Home", component: Dashboard }

]

const router = () => (
  <Router history={hist}>
    <Switch>
      {routes.map((prop, key) => {
        return <Route path={prop.path} component={prop.component} key={key} />;
      })}
    </Switch>
  </Router>
)

export default router;

