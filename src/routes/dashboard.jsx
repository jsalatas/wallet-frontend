import Dashboard from "pages/dashboard";
import Wallet from "pages/wallet";

// @material-ui/icons
import DashboardIcon from "@material-ui/icons/Dashboard";
import WalletIcon from "@material-ui/icons/AccountBalanceWallet";

var dashRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: DashboardIcon,
    component: Dashboard
  },
  {
    path: "/wallet",
    name: "Wallet",
    icon: WalletIcon,
    component: Wallet
  },
  { redirect: true, path: "/", pathTo: "/dashboard", name: "Dashboard" }
];
export default dashRoutes;
