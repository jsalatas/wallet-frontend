import React from 'react';
import PropTypes from 'prop-types';
import { isLoggedIn } from '../../../utils';
export class AuthenticatedComponent extends React.Component {
    static contextTypes = {
        router: PropTypes.object.isRequired,
    };
    componentWillMount() {
        if (!isLoggedIn()) {
            this.props.history.push('/login');
        }
    }
    render() {
        return null;
    }
}

export default AuthenticatedComponent;
