import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Grid from '@material-ui/core/Grid';

import  methods from './methods'

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
});

class SelectMethod extends React.Component {
  state = {
    selectedMethod: '',
  };

  handleListItemClick = (event, method) => {
    this.setState({ selectedMethod: method});
  };

  sendState() {
    return this.state;
  }

  isValidated() {
    return this.state.selectedMethod !== '';
  }

  render() {
    var items = methods.map(method => {
      return (
          <ListItem
            button
            disabled={method.disabled}
            selected={this.state.selectedMethod === method}
            onClick={event => this.handleListItemClick(event, method)}
            key={method.name}
          >
              <img height={40} width={120} alt='' src={require(`assets/img/${method.name}.png`)} />
          </ListItem>
      )
    })
    return (
      <Grid container justify="center">
        <List 
          component="nav"
          justify='center'
        >
          {items}
        </List>
      </Grid>
    );
  }
}

SelectMethod.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SelectMethod);