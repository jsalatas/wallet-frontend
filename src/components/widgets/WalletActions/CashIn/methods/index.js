import BtcIn from './BTC';
import FacIn from './FAC';

export const methods = [
    {name: 'bitcoin', Component: BtcIn},
    {name: 'faircoin', Component: FacIn },
    {name: 'creativecoin', Component: 'creativecoin', disabled: true},
    {name: 'sepa', Component: 'Sepa'},
    {name: 'easypay', Component: 'EasyPay'},
    {name: 'safetypay', Component: 'SafetyPay', disabled: true},
    {name: 'teleingreso', Component: 'Teleingreso', disabled: true}
]

export default methods