import  FAC_IN from './constants';

export default function fac_in(state = { status: 'initial', fac_in: {} }, action) {
  switch (action.type) {
    case FAC_IN : {
      const { 
        fac_in = state.fac_in,
        error = state.error,
        status = state.status
       } = action.payload
      return {
        fac_in,
        error,
        status,
      };
    }
    default:
      return state;
  }
}