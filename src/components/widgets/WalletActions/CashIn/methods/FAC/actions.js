import { createAction } from 'redux-actions';
import FAC_IN from './constants';
import { getAuthToken } from 'utils';
import axios from 'axios'


const reset = createAction(FAC_IN, () => ({
  status: 'initial',
}));

const begin = createAction(FAC_IN, () => ({
  status: 'pending',
}));

const fail = createAction(FAC_IN, error => ({
  error,
  status: 'error',
}));

const success = createAction(FAC_IN, fac_in=> ({
  fac_in,
  status: 'success'
}));

const getFacIn = () =>
  (dispatch) => {
    dispatch(begin);
    axios({
        method: 'GET',
        headers: {
        Authorization: `Bearer ${getAuthToken()}`
        },
        url: 'https://api.bankofthecommons.coop/company/v1/wallet/cash_in_token/fac-in'
    }).then(response => {
        let fac_in = response.data

        dispatch(success(fac_in.data))
    }).catch(error => {
        console.error("ERROR");
    });
}

const actions = {
    reset,
    begin,
    fail,
    success,
    getFacIn
}

export default actions;