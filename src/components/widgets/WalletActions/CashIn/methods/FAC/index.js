import React, { Component } from 'react';
import { connect } from 'react-redux';
import actions from './actions';
import { Paper } from '@material-ui/core';
import Table from "components/atoms/Table/Table.jsx";
import Close from "@material-ui/icons/Close";
import Button from "components/atoms/CustomButtons/Button.jsx";
import SweetAlert from "react-bootstrap-sweetalert";
import QRCode from 'qrcode.react'

class FacIn extends Component {
  constructor(){
      super();
      this.state = {
          loading: true,
          showQr: null
      };
  }

  componentWillMount() {
      this.props.getFacIn();
  }

  componentWillReceiveProps(nextProps){
      if(nextProps.status === "success") {
          this.setState({loading: false});
      }
  }
  hideQr() {
      this.setState({showQr: null})
  }
  showQr(element) {
    const  {token, label} = element
    const text =  "faircoin:"+ token.replace(/[^a-zA-Z 0-9.]+/g,' ') + "?amount=0&label=" + label.replace(/[^a-zA-Z 0-9.]+/g,' ');
    this.setState({
      showQr: (
        <SweetAlert
          style={{ display: "block", marginTop: "-200px" }}
          title={label ? label: 'Unlabeled'}
          onConfirm={() => this.hideQr()}
          onCancel={() => this.hideQr()}
        >
            <QRCode level='H' size={200} value={text} />
        </SweetAlert>
      )
    });
  }
  
  render() {
    const { fac_in } = this.props;
    let elements = []
    if(fac_in.elements){
        elements = fac_in.elements.map(element => ([
            element.id,
            element.label,
            element.token,
            <div>
                <Button onClick={() => this.showQr(element)}> Show Qr</Button>
                <Button color="danger" key={element.id}>
                    <Close />
                </Button>
            </div>
        ]))
    }
    return (
        <Paper>
            {this.state.showQr}
            <Table
                tableHead={["id","Label","Token", "Actions"]}
                tableData={elements}
            />
        </Paper>
    )
  }
}

const mapStateToProps = (state) => ({
    fac_in: state.fac_in.fac_in,
    status: state.fac_in.status
})

export default connect(mapStateToProps, actions)(FacIn);
