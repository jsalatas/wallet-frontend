import  BTC_IN_LIST from './constants';

export default function btc_in_list(state = { status: 'initial', list: {} }, action) {
  switch (action.type) {
    case BTC_IN_LIST : {
      const { 
        list = state.list,
        error = state.error,
        list_status = state.status
       } = action.payload
      return {
        list,
        error,
        list_status,
      };
    }
    default:
      return state;
  }
}