import { createAction } from 'redux-actions';
import BTC_IN_LIST from './constants';
import { getAuthToken } from 'utils';
import axios from 'axios'


const reset = createAction(BTC_IN_LIST, () => ({
  status: 'initial',
}));

const begin = createAction(BTC_IN_LIST, () => ({
  status: 'pending',
}));

const fail = createAction(BTC_IN_LIST, error => ({
  error,
  status: 'error',
}));

const success = createAction(BTC_IN_LIST, list=> ({
  list,
  status: 'success'
}));

const getBtcInList = () =>
  (dispatch) => {
    dispatch(begin);
    axios({
        method: 'GET',
        headers: {
        Authorization: `Bearer ${getAuthToken()}`
        },
        url: 'https://api.bankofthecommons.coop/company/v1/wallet/cash_in_token/btc-in'
    }).then(response => {
        let btc_in_list = response.data

        dispatch(success(btc_in_list.data))
    }).catch(error => {
        console.error("ERROR");
    });
}

const actions = {
    reset,
    begin,
    fail,
    success,
    getBtcInList
}

export default actions;