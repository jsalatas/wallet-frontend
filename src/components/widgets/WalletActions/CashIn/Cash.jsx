import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

import { Card } from "@material-ui/core";

const style = {
  infoText: {
    fontWeight: "300",
    margin: "10px 0 30px",
    textAlign: "center"
  },
  inputAdornmentIcon: {
    color: "#555"
  },
  inputAdornment: {
    position: "relative"
  }
};

class Cash extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  sendState() {
    return this.state;
  }
  isValidated() {
    return true;
  }
  render() {
    if(!this.props.allStates.choice){
      return <div></div>
    }
    let Component = ''
    if(this.props.allStates.choice.selectedMethod) {
      Component = this.props.allStates.choice.selectedMethod.Component
    }
    return (
      <Card>
        <div>
          {typeof(Component) === "string" ?  Component : <Component />}
        </div>
      </Card>
    );
  }
}
export default withStyles(style)(Cash);
