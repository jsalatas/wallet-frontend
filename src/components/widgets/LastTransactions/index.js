import React, { Component } from 'react';
import { connect } from 'react-redux';
import actions from './actions';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

class LastTransactions extends Component {
  constructor(){
      super();
      this.state = {
          loading: true
      };
  }

  componentWillMount() {
      this.props.getLastTransactions();
  }

  componentWillReceiveProps(nextProps){
      if(nextProps.status === "success") {
          this.setState({loading: false});
      }
  }
  render() {
    const { transactions = []} = this.props;
    const { loading } = this.state;
    if(loading && transactions.length === 0) {
        return "loading"
    }
    let transaction_list = transactions.map(trans => {
        const { 
            status,
            scale,
            amount,
            fee_info,
            type,
            total,
            updated,
            time_in,
            id
        } = trans
        let total_amount;
        var time;
        if (updated !== undefined){
            time = new Date(updated);
        }else{
            time = new Date(time_in);
        }
        var time_data_orig = new Date(time);
        time_data_orig.setTime( time_data_orig.getTime() - time_data_orig.getTimezoneOffset()*60*1000 );
        const time_string = time_data_orig.toUTCString().replace(" GMT", "");
        if(type === "fee") {
            total_amount = fee_info.amount / Math.pow(10, fee_info.scale).toFixed(fee_info.scale);
        } else {
            if (scale !== undefined){
                total_amount = (total / Math.pow(10, scale)).toFixed(scale);
            }else{
                total_amount = amount;
            }
        }
        return (
            <TableRow key={id}>
                <TableCell align="center">{status}</TableCell>
                <TableCell align="center">{total_amount}</TableCell>
                <TableCell align="center">{time_string.substring(5)}</TableCell>
                <TableCell align="center">{type}</TableCell>
            </TableRow>
        )
    })
    return (
        <Paper>
        <Table>
            <TableHead>
            <TableRow>
                <TableCell align="center">STATUS</TableCell>
                <TableCell align="center">AMOUNT</TableCell>
                <TableCell align="center">DATE</TableCell>
                <TableCell align="center">Service</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
                {
                    transaction_list
                }
            </TableBody>
        </Table>
        </Paper>
    )
  }
}

const mapStateToProps = (state) => ({
    transactions: state.last_transactions.transactions,
    status: state.last_transactions.status
})

export default connect(mapStateToProps, actions)(LastTransactions);
