import { createAction } from 'redux-actions';
import LAST_TRANSACTIONS from './constants';
import { getAuthToken } from 'utils';
import axios from 'axios'


const reset = createAction(LAST_TRANSACTIONS, () => ({
  status: 'initial',
}));

const begin = createAction(LAST_TRANSACTIONS, () => ({
  status: 'pending',
}));

const fail = createAction(LAST_TRANSACTIONS, error => ({
  error,
  status: 'error',
}));

const success = createAction(LAST_TRANSACTIONS, transactions => ({
  transactions,
  status: 'success'
}));

const getLastTransactions = () =>
  (dispatch) => {
    dispatch(begin);
    axios({
        method: 'GET',
        headers: {
        Authorization: `Bearer ${getAuthToken()}`
        },
        url: 'https://api.bankofthecommons.coop/user/v1/last'
    }).then(response => {
        let transactions = response.data
        dispatch(success(transactions.data))
    }).catch(error => {
        console.error("ERROR");
    });
}

const actions = {
    reset,
    begin,
    fail,
    success,
    getLastTransactions
}

export default actions;