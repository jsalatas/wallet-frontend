import  LAST_TRANSACTIONS from './constants';

export default function last_transactions(state = { status: 'initial' }, action) {
  switch (action.type) {
    case LAST_TRANSACTIONS : {
      const { 
        transactions = state.transactions,
        error = state.error,
        status = state.status
       } = action.payload
      return {
        transactions,
        error,
        status,
      };
    }
    default:
      return state;
  }
}