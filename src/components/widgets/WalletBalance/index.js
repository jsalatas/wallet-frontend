import React, { Component } from 'react'
import { connect } from 'react-redux';
import GridContainer from "components/atoms/Grid/GridContainer.jsx";
import GridItem from "components/atoms/Grid/GridItem.jsx";
import Card from "components/atoms/Card/Card.jsx";
import CardHeader from "components/atoms/Card/CardHeader.jsx";
import CardBody from "components/atoms/Card/CardBody.jsx";

class WalletBalance extends Component {
  constructor(){
      super()
      this.state = {
          loading: true,
      }
  }

  render() {
    const { balances } = this.props;
    let balances_list = ""
    if(balances.length > 0){
        balances_list = balances.map(balance => {
            const { id, currency, available, scale, status} = balance
            if(status === "enabled") 
                return (
                    <GridItem key={id} >
                            <CardBody>
                                {currency}
                                <br/>
                                {available/Math.pow(10, scale).toFixed(3)}
                            </CardBody>
                    </GridItem>
                )
        })
    }
    let total_balance = ""
    if(balances.length > 0 )  {
        const { id, currency, available, scale } = balances[balances.length-1]
        total_balance =   (
            <GridItem key={id} >
                    <CardBody>
                        Total balance {currency}
                        <br/>
                        {available/Math.pow(10, scale).toFixed(3)}
                    </CardBody>
            </GridItem>
        )
    }

    return (
        <div>
            <Card>
            <CardHeader>Wallet</CardHeader>
                <GridContainer>
                {total_balance}
                { balances_list }
                </GridContainer>
            </Card>
        </div>
    )
  }
}
const mapStateToProps = (state) => ({
    balances: state.wallet.data.length > 0 ? state.wallet.data : []
})
export default connect(mapStateToProps, {})(WalletBalance);
