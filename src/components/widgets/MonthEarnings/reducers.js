import  MONTH_EARNINGS from './constants';

export default function month_earnings(state = { status: 'initial' }, action) {
  switch (action.type) {
    case MONTH_EARNINGS : {
      const { 
        monthEarnings = state.monthEarnings,
        error = state.error,
        status = state.status
       } = action.payload
      return {
        monthEarnings,
        error,
        status,
      };
    }
    default:
      return state;
  }
}