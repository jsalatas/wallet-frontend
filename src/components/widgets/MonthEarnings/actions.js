import { createAction } from 'redux-actions';
import MONTH_EARNINGS from './constants';
import { getAuthToken } from 'utils';
import axios from 'axios'


const reset = createAction(MONTH_EARNINGS, () => ({
  status: 'initial',
}));

const begin = createAction(MONTH_EARNINGS, () => ({
  status: 'pending',
}));

const fail = createAction(MONTH_EARNINGS, error => ({
  error,
  status: 'error',
}));

const success = createAction(MONTH_EARNINGS, monthEarnings => ({
  monthEarnings,
  status: 'success'
}));

const getMonthEarnings = () =>
  (dispatch) => {
    dispatch(begin);
    axios({
        method: 'GET',
        headers: {
        Authorization: `Bearer ${getAuthToken()}`
        },
        url: 'https://api.bankofthecommons.coop/user/v1/wallet/monthearnings'
    }).then(response => {
        let monthEarnings = response.data
        dispatch(success(monthEarnings.data))
    }).catch(error => {
        console.error("ERROR");
    });
}

const actions = {
    reset,
    begin,
    fail,
    success,
    getMonthEarnings
}

export default actions;