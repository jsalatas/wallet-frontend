import React, { Component } from 'react';
import { connect } from 'react-redux';
import actions from './actions';
import CardHeader from "components/atoms/Card/CardHeader.jsx";
import Card from "components/atoms/Card/Card.jsx";
import CardBody from "components/atoms/Card/CardBody.jsx";
import { Line } from 'react-chartjs-2'

const MONTHS = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "Mai",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
]

class MonthEarnings extends Component {
  constructor(){
      super();
      this.state = {
          loading: true
      };
  }

  componentWillMount() {
      this.props.getMonthEarnings();
  }

  componentWillReceiveProps(nextProps){
      if(nextProps.status === "success") {
          this.setState({loading: false});
      }
  }
  render() {
    let { monthEarnings = []} = this.props;
    const { loading } = this.state;
    if(loading && monthEarnings.length === 0) {
        return "loading"
    }
    console.log(monthEarnings)
    monthEarnings = Object.values(monthEarnings).slice(0,12).map(earning => earning/Math.pow(10, monthEarnings.scale)).reverse()
    console.log(monthEarnings)
    let m = new Date().getMonth();
    let labels = MONTHS.slice(m+1).concat(MONTHS.slice(0, m+1))
    const data = {
        labels,
        datasets: [
          {
            label: 'Earning',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: monthEarnings
          }
        ]
      };
    return (
        <Card>
            <CardHeader>
                Month Earnings
            </CardHeader>
            <CardBody>
                <Line height={40} data={data} />
            </CardBody>
        </Card>
    )
  }
}

const mapStateToProps = (state) => ({
    monthEarnings: state.month_earnings.monthEarnings,
    status: state.month_earnings.status
})

export default connect(mapStateToProps, actions)(MonthEarnings);
