import  EARNINGS from './constants';

export default function earnings(state = { status: 'initial', earnings: {} }, action) {
  switch (action.type) {
    case EARNINGS : {
      const { 
        earnings = state.earnings,
        error = state.error,
        status = state.status
       } = action.payload
      return {
        earnings,
        error,
        status,
      };
    }
    default:
      return state;
  }
}