import React, { Component } from 'react';
import { connect } from 'react-redux';
import actions from './actions';
import CardHeader from "components/atoms/Card/CardHeader.jsx";
import CardBody from "components/atoms/Card/CardBody.jsx";
import Button from "components/atoms/CustomButtons/Button.jsx";
import { Popover, Paper } from '@material-ui/core';
import Refresh from "@material-ui/icons/Refresh";


class Earnings extends Component {
  constructor(){
      super();
      this.state = {
          loading: true,
          anchorEl: null
      };
  }


  componentWillMount() {
      this.props.getEarnings();
  }


  componentWillReceiveProps(nextProps){
      if(nextProps.status === "success") {
          this.setState({loading: false});
      }
  }

  handlePopoverOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handlePopoverClose = () => {
    this.setState({ anchorEl: null });
  };
  
  render() {
    const { earnings = {} } = this.props;
    const {  anchorEl } = this.state;
    let earningComponent = "";
    if(!earnings) {
        earningComponent = (<span>No data found</span>)
    } else {
        const { day, week, month, currency, scale = 0 } = earnings;
        earningComponent = (
            <CardBody>
                <div>Day: {day / Math.pow(10, scale).toFixed(scale)} {currency}</div>
                <div>Week: {week / Math.pow(10, scale).toFixed(scale)} {currency}</div>
                <div>Month: {month / Math.pow(10, scale).toFixed(scale)} {currency}</div>
            </CardBody>
        )
    }

    return (
    <div
          onMouseEnter={this.handlePopoverOpen}
          onMouseLeave={this.handlePopoverClose}
    >
        <Paper>
            <CardHeader>
                Earnings
            </CardHeader>
            {earningComponent}
        </Paper>
        <Popover
            anchorEl={anchorEl}
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            id="mouse-over-popover"
            open={Boolean(anchorEl)}
            onClose={this.handlePopoverClose}
        >
            <Button simple color="info" justIcon
                onClick={this.props.getEarnings}
            >
                <Refresh />
            </Button>
        </Popover>
    </div>
    )
  }
}

const mapStateToProps = (state) => ({
    earnings: state.earnings.earnings,
    status: state.earnings.status
})

export default connect(mapStateToProps, actions)(Earnings);
