import { createAction } from 'redux-actions';
import EARNINGS from './constants';
import { getAuthToken } from 'utils';
import axios from 'axios'


const reset = createAction(EARNINGS, () => ({
  status: 'initial',
}));

const begin = createAction(EARNINGS, () => ({
  status: 'pending',
}));

const fail = createAction(EARNINGS, error => ({
  error,
  status: 'error',
}));

const success = createAction(EARNINGS, earnings=> ({
  earnings,
  status: 'success'
}));

const getEarnings = () =>
  (dispatch) => {
    dispatch(begin);
    axios({
        method: 'GET',
        headers: {
        Authorization: `Bearer ${getAuthToken()}`
        },
        url: 'https://api.bankofthecommons.coop/user/v1/wallet/earnings'
    }).then(response => {
        let earnings = response.data

        dispatch(success(earnings.data))
    }).catch(error => {
        console.error("ERROR");
    });
}

const actions = {
    reset,
    begin,
    fail,
    success,
    getEarnings
}

export default actions;