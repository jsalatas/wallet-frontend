import  CURRENCIES_VALUE from './constants';

export default function currencies_value(state = { status: 'initial' }, action) {
  switch (action.type) {
    case CURRENCIES_VALUE : {
      const { 
        values = state.values,
        error = state.error,
        status = state.status
       } = action.payload
      return {
        values,
        error,
        status,
      };
    }
    default:
      return state;
  }
}