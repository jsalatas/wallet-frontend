import React, { Component } from 'react'
import actions from './actions';
import { connect } from 'react-redux';
import GridContainer from "components/atoms/Grid/GridContainer.jsx";
import GridItem from "components/atoms/Grid/GridItem.jsx";
import Card from "components/atoms/Card/Card.jsx";
import CardHeader from "components/atoms/Card/CardHeader.jsx";
import CardBody from "components/atoms/Card/CardBody.jsx";

class CurrenciesValue extends Component {
  constructor(){
      super()
      this.state = {
          loading: true,
      }
  }

  componentWillMount(){
      this.props.getValues();
  }

  componentWillReceiveProps(nextProps){
      if(nextProps.status === "success"){
          this.setState({ loading: false});
      }

  }
  render() {
    const { values = {} } = this.props;
    if(this.state.loading && Object.keys(values).length === 0) return "Loading";
    let values_list = Object.keys(values).map( key => {
        return (
          <GridItem key={key} >
                <CardBody>
                    {key}
                    <br/>
                    {values[key]}
                </CardBody>
          </GridItem>
        )
    })

    return (
        <div>
            <Card>
            <CardHeader>Currencies value</CardHeader>
                <GridContainer>
                { values_list }
                </GridContainer>
            </Card>
        </div>
    )
  }
}
const mapStateToProps = (state) => ({
    values: state.currencies_value.values,
    status: state.currencies_value.status
})
export default connect(mapStateToProps, actions)(CurrenciesValue);
