import { createAction } from 'redux-actions';
import CURRENCIES_VALUE from './constants';
import { getAuthToken } from 'utils';
import axios from 'axios'


const reset = createAction(CURRENCIES_VALUE, () => ({
  status: 'initial',
}));

const begin = createAction(CURRENCIES_VALUE, () => ({
  status: 'pending',
}));

const fail = createAction(CURRENCIES_VALUE, error => ({
  error,
  status: 'error',
}));

const success = createAction(CURRENCIES_VALUE, values => ({
  values,
  status: 'success'
}));

const getValues = () =>
  (dispatch) => {
    dispatch(begin);
    axios({
        method: 'GET',
        headers: {
        Authorization: `Bearer ${getAuthToken()}`
        },
        url: 'https://api.bankofthecommons.coop/exchange/v1/ticker/eur'
    }).then(response => {
        let values = response.data
        dispatch(success(values.data))
    }).catch(error => {
        console.error("ERROR");
    });
}

const actions = {
    reset,
    begin,
    fail,
    success,
    getValues
}

export default actions;