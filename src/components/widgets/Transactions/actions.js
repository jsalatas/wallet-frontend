import { createAction } from 'redux-actions';
import TRANSACTIONS from './constants';
import { getAuthToken } from 'utils';
import axios from 'axios'


const reset = createAction(TRANSACTIONS, () => ({
  status: 'initial',
}));

const begin = createAction(TRANSACTIONS, () => ({
  status: 'pending',
}));

const fail = createAction(TRANSACTIONS, error => ({
  error,
  status: 'error',
}));

const success = createAction(TRANSACTIONS, transactions => ({
  transactions,
  status: 'success'
}));

const getTransactions = () =>
  (dispatch) => {
    dispatch(begin);
    axios({
        method: 'GET',
        headers: {
        Authorization: `Bearer ${getAuthToken()}`
        },
        url: 'https://api.bankofthecommons.coop/user/v1/last'
    }).then(response => {
        let transactions = response.data
        dispatch(success(transactions.data))
    }).catch(error => {
        console.error("ERROR");
    });
}

const actions = {
    reset,
    begin,
    fail,
    success,
    getTransactions
}

export default actions;