import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";

// @material-ui/icons
import Dvr from "@material-ui/icons/Dvr";
import Favorite from "@material-ui/icons/Favorite";
import Close from "@material-ui/icons/Close";

import Paper from '@material-ui/core/Paper';
// core components
import Button from "components/atoms/CustomButtons/Button.jsx";
import { connect } from 'react-redux'
import actions from './actions'

class Transactions extends Component {
  constructor(){
      super();
      this.state = {
          loading: true
      };
  }

  componentWillMount() {
      this.props.getTransactions();
  }

  componentWillReceiveProps(nextProps){
      if(nextProps.status === "success") {
          this.setState({loading: false});
      }
  }
  render() {
    const { transactions = []} = this.props;
    const { loading } = this.state;
    if(loading && transactions.length === 0) {
        return "loading"
    }
    let transaction_list = transactions.map(trans => {
        const { 
            status,
            scale,
            amount,
            fee_info,
            type,
            total,
            updated,
            time_in,
            id
        } = trans
        let total_amount;
        var time;
        if (updated !== undefined){
            time = new Date(updated);
        }else{
            time = new Date(time_in);
        }
        var time_data_orig = new Date(time);
        time_data_orig.setTime( time_data_orig.getTime() - time_data_orig.getTimezoneOffset()*60*1000 );
        const time_string = time_data_orig.toUTCString().replace(" GMT", "");
        if(type === "fee") {
            total_amount = fee_info.amount / Math.pow(10, fee_info.scale).toFixed(fee_info.scale);
        } else {
            if (scale !== undefined){
                total_amount = (total / Math.pow(10, scale)).toFixed(scale);
            }else{
                total_amount = amount;
            }
        }

        return ({
            id,
            status,
            amount: total_amount,
            date: time_string.substring(5),
            service: type,
            actions: (
              // we've added some custom button actions
              <div className="actions-right">
                { /* use this button to add a like kind of action */ }
                <Button
                  justIcon
                  round
                  simple
                  onClick={() => alert("You've pressed the like button on colmun id: ")}
                  color="info"
                  className="like"
                >
                  <Favorite />
                </Button>{" "}
                { /* use this button to add a edit kind of action */ }
                <Button
                  justIcon
                  round
                  simple
                  onClick={() => alert("You've pressed the edit button on colmun id: ")}
                  color="warning"
                  customClass="edit">
                  <Dvr />
                </Button>{" "}
                { /* use this button to remove the data row */ }
                <Button
                  justIcon
                  round
                  simple
                  onClick={() => alert("You've pressed the delete button on colmun id: ")}
                  color="danger"
                  customClass="remove">
                  <Close />
                </Button>{" "}
              </div>
            )
          })

    })
    return (
        <Paper>
    <ReactTable
      data={transaction_list}
      filterable
      columns={[
        {
          Header: "Status",
          accessor: "status",
        },
        {
          Header: "Amount",
          accessor: "amount"
        },
        {
          Header: "Date",
          accessor: "date"
        },
        {
          Header: "Service",
          accessor: "service"
        },
        {
          Header: "Actions",
          accessor: "actions",
          sortable: false,
          filterable: false,
        }
      ]}
      defaultPageSize={10}
      showPaginationTop
      showPaginationBottom={false}
      className="-striped -highlight"
    />
        </Paper>
    )
  }
}

const mapStateToProps = (state) => ({
    transactions: state.transactions.transactions,
    status: state.transactions.status
})

export default connect(mapStateToProps, actions)(Transactions);
