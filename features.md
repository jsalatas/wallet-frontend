# Bank of commons ReactJS frontend

## Screens
1. register
2. login
3. forgot password
4. company
    1. account
    2. users
    3. settings
5. user
    1. company
    2. profile
    3. security
    4. clients
6. dashboard
7. wallet
8. pos
9. about

## Project init
- Integration Material Ui Admin Theme

## Register
- registration form
- Show successed message

## Login
- Login form
- handle error

## Forgot password
- Forgot password form
- Show successed message

## Company account
#### Info widgets
- Account information
    1. Change Logo
    2. Reset Keys
    3. Edit company info
- Responsible of Company KYC data
- Api Data
- Tier level
- Cash in methods
- Cash out methods
- Exchanges

## Company users
- pagination widget of company users
- Add new user
    1. Add existing user
    2. Create new user

## Company settings
#### Checkbox widgets
- Activity notification
- currencies

#### My schedules widget
- pagination widget of schedules
- Add new (4 step wizard)

## User company
- List of in company

## User profile
- Change photo
- Basic info form
- Change password
- Edit profile info

## User security and 2FA
- Show-Hide widget
- Reload qr code

## My account clients
- pagination widget of clients

## Dashboard
- Choice reference valute
- Currencies Value
- Graph of activity
- List of last transactions
- List of alerts
- Status panel

## Wallet
- Choice reference valute
- Info widget of current total balance
- Cash in
- Cash out
- Exchange
- Wallet to wallet
- Wallet transacrions widget
    1. pagination
    2. graph
    3. filters
    4. list of transactions

## Pos
- Pagination list widget
- Add new pos

## About
- Info widget
